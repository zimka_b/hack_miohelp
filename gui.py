#from pyqtgraph.Qt import QtGui, QtCore
import numpy as np
import pyqtgraph as pg
import random
from config import N_SIGNALS


class BasicPlotter(object):
    def __init__(self, core):
        self.core = core

        self.app = pg.QtGui.QApplication([])

        self.win = pg.GraphicsWindow(title="Plot singals")
        self.win.resize(1000, 600)
        self.win.setWindowTitle('Plotting')

        self.alert = 0

        # Enable antialiasing for prettier plots
        pg.setConfigOptions(antialias=True)
        self.p = [self.win.addPlot(title="Signal {}".format(n)) for n in range(N_SIGNALS)]
        self.curves = [ self.p[n].plot(pen='w') for n in range(N_SIGNALS)]
        for p_i in self.p:
            p_i.enableAutoRange('xy', True)
            #p_i.setYRange(0, 1024, padding=0)

        self.timer = pg.Qt.QtCore.QTimer()
        self.timer.timeout.connect(self.update)

    def update(self):
        current_data, current_class = self.core.get_data_n_class()
        for n in range(N_SIGNALS):
            self.curves[n].setData(current_data[:,n])
        self.changeColor(current_class)

    def run(self, num=200):
        self.timer.start(200)
        import sys
        if sys.flags.interactive != 1 or not hasattr(pg.QtCore, 'PYQT_VERSION'):
            pg.QtGui.QApplication.exec_()

    def exit(self):
        print("exit?")
        self.core.save()

    def changeColor(self, alert):
        if alert == self.alert:
            return
        self.alert = alert
        color = 'r' if self.alert else 'w'
        for curve in self.curves:
            curve.setData(pen=pg.mkPen(color))
