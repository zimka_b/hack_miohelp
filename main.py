import sys
from core import Core
from gui import BasicPlotter
from source import FakeDataSource, WrapProcessSource, MyoDataSource
from config import N_SIGNALS
from util import MultiContainer

fake_wrap = lambda x, args: x(*args)
def main():
    with WrapProcessSource(MyoDataSource, [N_SIGNALS]) as source:
        current_core = Core(source)
        app = BasicPlotter(current_core)
        app.run()
        print("saving...")
        app.exit()

if __name__ == '__main__':
    main()
