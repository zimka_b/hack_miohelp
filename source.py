import random
import numpy as np
from multiprocessing import Process, Queue
import serial
import time
from config import R_SIZE, R_DELTA, END, N_MYO_SIZE, COMPORT

class MyoDataSource(object):
    r_size = R_SIZE
    r_delta = R_DELTA
    cap = 65535
    byte_size = 2
    def __init__(self, signals):
        self.signals = signals

    def __enter__(self):
        # open COM-port
        self.ser = serial.Serial(COMPORT, 115200, timeout=1)
        return self

    def __exit__(self, *args):
        # close COM-port
        self.ser.close()
        return

    def get_aligned(self, data_base):
        """Try to align along FF, else return as is"""
        data = data_base.copy().reshape(-1)
        index = np.where(data == self.cap)
        if len(index) == 1:
            index = index[0]
            if len(index) > 1:
                left = index[0] + 1
                right = index[-1] + 1
                try:
                    data = data[left:right]
                    data = data.reshape(-1, self.signals + 1)
                    data = data[:, 0:self.signals]
                    print("fixshape", data.shape)
                    return data
                except:
                    pass
        print("failed to align", data_base.shape )
        return data_base[:,0:self.signals]

    def read_till_cap(self):
        tried = []
        for n in range(2*self.signals):
            sl = 0
            while self.ser.in_waiting < self.byte_size:
                time.sleep(0.05)
            line = self.ser.read(self.byte_size)
            buf = np.frombuffer(bytearray(line), dtype='>u2')
            tried.append(buf)
            if buf[0] == self.cap:
                print("Fixed", n)
                return
        raise ValueError("Can't fix:{}".format(tried))

    def fix_read(self, data):
        data = self.get_aligned(data)
        self.read_till_cap()
        return data

    def get(self):
        n_bytes = self.byte_size*(self.signals + 1)
        sl = 0
        while self.ser.in_waiting < n_bytes:
            sl += 1
            time.sleep(0.05)

        can_load = n_bytes*(self.ser.in_waiting // n_bytes)
        line = self.ser.read(can_load)
        buf = np.frombuffer(bytearray(line), dtype='>u2')
        full = buf.reshape(-1,self.signals+1)
        last = full[:, -1]
        valid = np.all(last == self.cap)
        if valid:
            return full[:, 0:5]
        else:
            return self.fix_read(full)


class FakeDataSource(object):
    r_size = R_SIZE
    r_delta = R_DELTA
    def __init__(self, signals):
        self.signals = signals


    def __enter__(self):
        # open COM-port
        return self

    def __exit__(self, *args):
        # close COM-port
        return

    def get(self):
        l = random.randint(self.r_size - self.r_delta, self.r_size + self.r_delta)
        data = np.random.random((l, self.signals))
        data[0] *= 5
        return data


def run_source(source_cls, source_args, data_queue, cancel_queue):
    with source_cls(*source_args) as source:
        msg = None
        while not msg:
            if not data_queue.empty():
                continue
            d = source.get()
            if d is None:
                continue
            data_queue.put(d, False)
            if not cancel_queue.empty():
                msg = cancel_queue.get(False)
            if msg:
                if (msg != END):
                    print("ERROR: non end message {}".format(msg))
                break
    print("Finish source")


class WrapProcessSource(object):
    def __init__(self, source_cls, source_args):
        self.data_queue = Queue()
        self.cancel_queue = Queue()
        self.process = Process(target=run_source, args=(source_cls, source_args, self.data_queue, self.cancel_queue,))
        self.process.daemon = True

    def __enter__(self):
        self.process.start()
        data = self.data_queue.get()
        return self

    def __exit__(self, *args):
        self.cancel_queue.put(END)
        self.process.join()

    def get(self):
        empty =  self.data_queue.empty()
        ds = []
        while not empty:
            d = self.data_queue.get(False)
            ds.append(d)
            empty = self.data_queue.empty()
        if not ds:
            return None
        try:
            return np.vstack(ds)
        except Exception as e:
            print([x.shape for x in ds])
            raise e
