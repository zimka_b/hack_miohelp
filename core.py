from datetime import datetime
from util import DataInternal, MultiContainer
from model import MultiSignalClassifier
from extractor import Sampler, FeatureExtractor
from config import N_SIGNALS, DATA_PATH, SAVED_ENSEMBLE_FILENAME, WINDOW_SIZE, M_WINDOWS_SHOW, CLASSIFICATION_PERIOD_MS


class Core:
    def __init__(self, data_source):
        self.source = data_source
        self.data = DataInternal(signals=N_SIGNALS, path=DATA_PATH)
        self.classifier = MultiSignalClassifier(ensemble_filename=SAVED_ENSEMBLE_FILENAME)
        self._current_class = 0
        self._last_time_class_measure = datetime.now()

    def get_data_n_class(self):
        self.data.push(self.source.get())
        self._update_class()
        return self.data.pull(M_WINDOWS_SHOW*WINDOW_SIZE), self._current_class

    def _update_class(self):
        time_passed_ms = (datetime.now() - self._last_time_class_measure).total_seconds() * 1000
        if time_passed_ms < CLASSIFICATION_PERIOD_MS:
            return
        multi_data = self._build_features()
        if multi_data is None:
            return
        self._current_class = self.classifier.predict(multi_data)
        self._last_time_class_measure = datetime.now()

    def _build_features(self):
        sampler = Sampler(WINDOW_SIZE)
        window = self.data.pull(2*WINDOW_SIZE)
        multi_dict = {}
        for n in range(N_SIGNALS):
            data = sampler.get(window[:,n])
            if not len(data):
                return
            ext = FeatureExtractor(data)
            wavelet_features = ext.get_wavelet_features()
            key = "w{}".format(n)
            multi_dict[key] = wavelet_features
            arima_features = ext.get_arima_features()
            if arima_features is None:
                # we failed this time, try next time
                print("arima failed!")
                return
            key = "a{}".format(n)
            multi_dict[key] = arima_features
        return MultiContainer(multi_dict)

    def save_data(self):
        self.data.compile_total_data()
