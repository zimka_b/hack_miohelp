R_SIZE = 30
R_DELTA = 10
END = 'END'

N_MYO_SIZE = 10
ISWIN = False

WINDOW_SIZE = 256
M_WINDOWS_SHOW = 4
N_SIGNALS = 5
DATA_PATH = "data/"
COLORS = ['w', 'w', 'w', 'w', 'w']

ARIMA_SETUP = {
        "p": 5,
        "q": 0,
        "d": 1
}
WAVELET_SETUP = {
        "basis": "bior3.5",
        "level_drop": 3,
        "cut_redundant": False
}

BUFFER_SIZE = 2500
CLASSIFICATION_PERIOD_MS = 5000
SAVED_ENSEMBLE_FILENAME = "storage/classifier.ensemble"

if ISWIN:
    COMPORT = "COM4"
else:
    COMPORT = '/dev/ttyUSB0'
