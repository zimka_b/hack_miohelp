import random
import os
from collections import OrderedDict
import numpy as np
from config import BUFFER_SIZE

class DataInternal(object):
    def __init__(self, signals=3, size=BUFFER_SIZE, path=""):
        self.ptr = 0
        self.drop_index = 0
        self.hash_name = ("%32x" % random.getrandbits(32)).strip()
        self.path = path
        self.data = np.zeros((size, signals))

    def pull(self, window_size):
        if window_size > len(self.data):
            raise ValueError("Increase data size or reduce window size")
        if window_size > self.ptr:
            window_size = self.ptr
        left = self.ptr - window_size
        right = self.ptr
        return self.data[left:right, :]

    def get_available_space(self):
        return len(self.data) - self.ptr

    def drop_data(self):
        index = self.drop_index
        half_size = len(self.data)//2
        if self.path is not None:
            name = "{}{}_{}.npy".format(self.path, self.hash_name, index)
            np.save(name, self.data[0:half_size])
        new_data = np.zeros(self.data.shape)
        new_data[0:half_size] = self.data[half_size::]
        self.data = new_data
        self.drop_index += 1
        self.ptr -= half_size

    def push(self, data):
        if data is None:
            return
        if data.shape[1] != self.data.shape[1]:
            raise ValueError("Broken shape of signal")
        need_space = len(data)
        have_space = self.get_available_space()
        if have_space < need_space:
            self.drop_data()
            have_space = self.get_available_space()
            if have_space < need_space:
                raise ValueError("Increase data size, have {}, need {}".format(have_space, need_space))

        self.data[self.ptr:self.ptr + need_space] = data
        self.ptr += need_space

    def shape(self):
        return self.data.shape

    def compile_total_data(self):
        path = self.path if self.path else '.'
        names = sorted([x for x in os.listdir(path) if self.hash_name in x])
        datas = []
        for n in names:
            datas.append(np.load(self.path + n))
        datas.append(self.data[len(self.data)/2, :])
        total = np.vstack(datas)
        np.savetxt(
            '{}{}_total.csv'.format(self.path, self.hash_name),
            total,
            delimiter=',',
            newline='\n',
            fmt='%10.5f'
        )


class MultiContainer:
    FIRST_KEY_PART = ("a", "w")
    SECOND_KEY_PART = list(range(5))

    def __init__(self, data={}):
        valid_keys = []
        for first in self.FIRST_KEY_PART:
            for second in self.SECOND_KEY_PART:
                valid_keys.append("{}{}".format(first, second))
        self._data = OrderedDict((key, data.get(key, [])) for key in valid_keys)

    def keys(self):
        return list(self._data.keys())

    def _check_key(self, key):
        if not key in self.keys():
            raise KeyError("Key '{}' is invalid".format(key))

    def __getitem__(self, key):
        self._check_key(key)
        return self._data[key]

    def __setitem__(self, key, value):
        self._check_key(key)
        self._data[key] = value
        return True

    def __len__(self):
        lengths = [len(self._data[key]) for key in self.keys()]
        unique_lengths = set(lengths)
        if len(unique_lengths) == 1:
            return unique_lengths.pop()
        else:
            return lengths
