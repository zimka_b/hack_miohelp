import pickle
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.naive_bayes import GaussianNB
from xgboost.sklearn import XGBClassifier
import numpy as np
from util import MultiContainer


class MultiSignalClassifier:
    def __init__(self, ensemble_filename=None, rf_options={}, knn_options={}, xgb_options={}):
        if ensemble_filename is None:
            self._build_ensemble(rf_options, knn_options, xgb_options)
        else:
            self._load_ensemble(ensemble_filename)

    def fit(self, multi_train_data, multi_train_label):
        for key in self.ensemble.keys():
            for clf in self.ensemble[key]:
                clf.fit(X=multi_train_data[key], y=multi_train_label[key])

    def predict(self, multi_data):
        labels = []
        for key in self.ensemble.keys():
            for clf in self.ensemble[key]:
                try:
                    labels.append(clf.predict(multi_data[key]))
                except Exception as e:
                    print(key)
                    print(multi_data[key])
                    raise e
        print(labels)
        print(np.average(labels,axis=0))
        print("============")
        return (np.average(labels, axis=0) > 0.2)

    def get_classifier(self, key):
        return self.ensemble[key]

    def _load_ensemble(self, ensemble_filename):
        with open(ensemble_filename, "rb") as f:
            self.ensemble = pickle.loads(f.read())

    def _build_ensemble(self, rf_options, knn_options, xgb_options):
        clf_w0_0 = RandomForestClassifier(**rf_options)
        clf_w0_1 = KNeighborsClassifier(**knn_options)

        clf_w1_0 = RandomForestClassifier(**rf_options)
        clf_w1_1 = GaussianNB()

        clf_w2_0 = RandomForestClassifier(**rf_options)
        clf_w2_1 = GaussianNB()

        clf_w3_0 = RandomForestClassifier(**rf_options)
        clf_w3_1 = GaussianNB()
        # Don't user w4 - gets weak scores

        clf_a0_0 = XGBClassifier()
        clf_a0_1 = KNeighborsClassifier(**knn_options)

        clf_a1_0 = XGBClassifier()
        clf_a1_1 = KNeighborsClassifier(**knn_options)

        clf_a2_0 = XGBClassifier()
        clf_a2_1 = KNeighborsClassifier(**knn_options)

        clf_a3_0 = XGBClassifier()
        clf_a3_1 = KNeighborsClassifier(**knn_options)

        clf_a4_0 = XGBClassifier()
        clf_a4_1 = KNeighborsClassifier(**knn_options)

        cal = lambda x: CalibratedClassifierCV(x)
        m = MultiContainer()
        m['w0'] = [cal(clf_w0_0), cal(clf_w0_1)]
        m['w1'] = [cal(clf_w1_0), cal(clf_w1_1)]
        m['w2'] = [cal(clf_w2_0), cal(clf_w2_1)]
        m['w3'] = [cal(clf_w3_0), cal(clf_w3_1)]

        m['a0'] = [clf_a0_0, cal(clf_a0_1)]
        m['a1'] = [clf_a1_0, cal(clf_a1_1)]
        m['a2'] = [clf_a2_0, cal(clf_a2_1)]
        m['a3'] = [clf_a3_0, cal(clf_a3_1)]
        m['a4'] = [clf_a4_0, cal(clf_a4_1)]
        self.ensemble = m
