import pywt
import numpy as np
import pandas as pd
from scipy.signal import argrelmax
from statsmodels.tsa.arima_model import ARIMA
import pdb;
from config import ARIMA_SETUP, WAVELET_SETUP


class FeatureExtractor:
    """
    For given 1d window-sized sample computes arima and
    wavelet_features.
    """

    def __init__(self, sample):
        self.sample = sample

    def get_arima_features(self):
        """
        Returns p+q+d+1 features + 2 (mean/std)
        """
        p = ARIMA_SETUP['p']
        q = ARIMA_SETUP['q']
        d = ARIMA_SETUP['d']
        signal = self._get_derivative(self.sample.as_matrix())
        try:
            model = ARIMA(signal, order=(p,q,d)).fit(disp=0, max_iter=100)
            params = model.params
            if np.isnan(params).any():
                return
            if np.mean(model.resid) != np.mean(model.resid):
                print("WOW!")
                print(signal)
                print(params)
                print(model.resid)
                print(model.predict())
                raise ValueError("bad!")
            extra = np.array([np.mean(model.resid), np.std(model.resid)])
            return pd.DataFrame(np.hstack((params, extra)).reshape(1, -1))
        except Exception as e:
            print("Arima Exc!")

    def get_wavelet_features(self):
        """
        Returns numpy array of 10 (or 6) wavelet features.
        """
        data = self.sample
        if not isinstance(data, np.ndarray):
            data = data.as_matrix().reshape(-1)
        operator = pywt.Wavelet(WAVELET_SETUP.get('basis'))
        level = int(np.log2(len(data))) - WAVELET_SETUP.get('level_drop')
        coeffsApprox = []
        coeffsDetail = []
        approx = data
        for i in range(level):
            (approx, detail) = pywt.dwt(approx, operator, mode='per')
            coeffsApprox.append(approx)
            coeffsDetail.append(detail)

        result = []
        for x in self._extract_features_from_subband(coeffsDetail[-1]):
            result.append(x)

        for x in self._extract_features_from_subband(coeffsApprox[-1]):
            result.append(x)
        return pd.DataFrame(np.array(result).reshape(1, -1))

    @staticmethod
    def _get_derivative(signal):
        return signal[1::] - signal[:-1]

    @staticmethod
    def _extract_features_from_subband(data):
        """
        For array-like data return:
            number_of_zero_crossings,
            number_of_local_maximums,
            mean_absolute_value,
            root_mean_square,
            standart_deviation,
        """
        zero_counter = (data[1::]*data[:-1] < 0).sum()
        max_indices,  = argrelmax(data)
        max_counter = len(max_indices)
        mean_square = np.sqrt(np.sum(data**2))

        if WAVELET_SETUP.get('cut_redundant'):
            return (zero_counter, max_counter, mean_square)
        mean_mod = np.mean(np.abs(data))
        standard_dev = np.std(data)
        return (zero_counter, max_counter, mean_mod, mean_square, standard_dev)


class Sampler:
    def __init__(self, wsize, overlap=0.5, ewm_alpha=0.05, spike_thr=10000):
        self.wsize = wsize
        self.overlap = overlap
        self.step = int(wsize * (1. - overlap))
        self.ewm_alpha = ewm_alpha
        self.spike_thr = spike_thr

    def get(self, signal, number=-2):
        max_number = self.max_number(len(signal))
        out_pos = number >= 0 and number > max_number
        out_neg = number < 0 and number < -max_number - 1
        if (out_pos or out_neg):
            return signal[0:0]
        if number >= 0:
            start = self.step * number
        else:
            start = len(signal) - self.wsize + (number + 1) * self.step
        stop = start + self.wsize
        support_length = int(1./self.ewm_alpha) - 1
        with_support = support_length > 0 and (start - support_length) > 0
        if with_support:
            data = signal[start - support_length:stop]
            return self._smooth(data)[support_length:]
        else:
            return self._smooth(signal[start:stop])

    def _smooth(self, data):
        new_data = pd.DataFrame(data)
        for c in new_data.columns:
            ind = new_data[c] > self.spike_thr
            med = new_data[c].median()
            new_data.loc[ind, c] = med
        return new_data.ewm(alpha=self.ewm_alpha).mean()

    def max_number(self, length):
        if length < self.wsize:
            return 0
            #raise ValueError("Signal({}) is too short for window {}".format(length, self.wsize))
        return (length - self.wsize) // self.step
